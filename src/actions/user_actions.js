import axios from 'axios';
import {browserHistory} from 'react-router';
import {
	AUTH_USER, UNAUTH_USER, AUTH_USER_ERROR, FETCH_USER
		} from "./types";
var ROOT_API_URL = "/api"

export function signUpFunction(user, trialOrPurchaseCheck) {
	return function(dispatch) {
		var url = "/";
		if (trialOrPurchaseCheck) {
			url = trialOrPurchaseCheck === "TRY" ? "tryItOptions" : "buyItOptions";
		}
		axios.post(`${ROOT_API_URL}/user/signup`, user)
		.then(function(res) {
			localStorage.setItem("token", res.data.token)
			dispatch({type: AUTH_USER});
			browserHistory.push(url);
		})
		.catch(function(err) {
			dispatch({type: AUTH_USER_ERROR, payload: err.message})
		});

	}
}

export function loginFunction(user) {
	return function(dispatch) {
		axios.get(`${ROOT_API_URL}/user/signin`, user)
			.then(function(res) {
				localStorage.setItem("token", res.data.token)
				dispatch({type: AUTH_USER});
			})
			.catch(function(err) {
				dispatch({type: AUTH_USER_ERROR, payload: err.message})
			});
	}
}
export function signout() {
	return function(dispatch) {
		localStorage.removeItem("token");
		dispatch({type: UNAUTH_USER})
	}
}

export function fetchUser() {
	return function(dispatch) {
		const config = {headers: {authorization: localStorage.getItem('token')}}
		axios.get(`${ROOT_API_URL}/user/profile`, config)
		.then(function(res) {
			dispatch({type: FETCH_USER, payload: res.data.user});
		})
		.catch(function(err) {
			dispatch({type: AUTH_USER_ERROR, payload: err.message})
		})
	}
}
