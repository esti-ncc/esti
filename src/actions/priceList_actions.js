import axios from 'axios';
import {
	FETCH_DEFAULT_PRICELIST_ERROR, FETCH_DEFAULT_PRICELIST, SET_PRICELIST_SELECTED
		} from "./types";
var ROOT_API_URL = "/api";

export function getPriceList() {
		return function(dispatch) {
      axios.get(`${ROOT_API_URL}/priceList/default`)
      .then(function(res) {
        dispatch({"type": FETCH_DEFAULT_PRICELIST, "payload": res.body.priceList})
      })
      .catch(function(res) {
        dispatch({"type": FETCH_DEFAULT_PRICELIST_ERROR})
      })
    }
}

export function setPriceListSelected(id) {
  return function(dispatch) {
    dispatch("type": SET_PRICELIST_SELECTED, "payload": id);
  }
}
