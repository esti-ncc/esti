import {browserHistory} from 'react-router';

export function confirmation() {
	return function(dispatch) {
		var r = confirm("Have you downloaded your current estimate? If you do not do so before continuing, your data will be lost. Do you wish to continue?");
		if (r === true) {
		  browserHistory.push('/new');
			dispatch({type: "none"});
		} else {
			dispatch({type: "none"});
		}
	}
}
