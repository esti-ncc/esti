import React, { Component } from 'react';
import { Link } from 'react-router';
import '../index.css';
import Navbar from './Navbar.jsx';

class BuyItOptions extends Component {
  render() {
    return (
    <div>
    	<Navbar />
	    <div className="container col-md-6 col-md-offset-3 m-t-xl m-b-xl">
			<div className="panel panel-default drop_shadow solid_panel">
			  <div className="text-center"><i className="fa fa-globe" aria-hidden="true"></i> Estimation Online</div>
			  <div className="panel-body wrapper-xl">
			    <h1 className="text-primary">Subscription Options</h1>
			    <hr/>
			    <p>
			    	For your convenience we offer two different subscription options. When you subscribe to our software you can depend on the fact that you will always have the latest version of Estimation Online. 
			    </p>
			    <br/>
			    <em ><strong>- The Estimation Online Team</strong></em>
			    <hr/>
			    <div className="form-group">
					    <label className="label">Select an option below</label>
					    <div className="row">
					    	<div className="col-md-6">
							    <div className="radio text-center">
							    	<div><i className="fa fa-calendar-minus-o fa-4x padder-v text-primary" aria-hidden="true"></i></div>
									  <label><input type="radio" name="optradio"/>$9/Month</label>
									</div>
								</div>
								<div className="col-md-6">
									<div className="radio text-center">
										<div><i className="fa fa-calendar fa-4x padder-v text-primary" aria-hidden="true"></i></div>
									  <label><input type="radio" name="optradio"/>$99/Year</label>
									</div>
								</div>
							</div>
							<label className="label">Please enter your payment information</label>
							  <div className="row">
							  <form className="padder-v" role="form">
							    <fieldset>
							      <div class="form-group">
							        <div className="col-md-12 m-b-sm">
							          <input type="text" className="form-control" name="card-holder-name" id="card-holder-name" placeholder="Name"/>
							        </div>
							      </div>
							      <div className="form-group">
							        <div className="col-md-12 m-b-sm">
							          <input type="text" className="form-control" name="card-number" id="card-number" placeholder="Card Number"/>
							        </div>
							      </div>
							      <div className="form-group">
							        <div className="">
							            <div className="col-md-4">
							              <select className="form-control" name="expiry-month" id="expiry-month">
							                <option>Month</option>
							                <option value="01">Jan (01)</option>
							                <option value="02">Feb (02)</option>
							                <option value="03">Mar (03)</option>
							                <option value="04">Apr (04)</option>
							                <option value="05">May (05)</option>
							                <option value="06">June (06)</option>
							                <option value="07">July (07)</option>
							                <option value="08">Aug (08)</option>
							                <option value="09">Sep (09)</option>
							                <option value="10">Oct (10)</option>
							                <option value="11">Nov (11)</option>
							                <option value="12">Dec (12)</option>
							              </select>
							            </div>
							            <div className="col-md-4">
							              <select className="form-control" name="expiry-year">
							                <option value="17">2017</option>
							                <option value="18">2018</option>
							                <option value="19">2019</option>
							                <option value="20">2020</option>
							                <option value="21">2021</option>
							                <option value="22">2022</option>
							                <option value="23">2023</option>
							                <option value="13">2024</option>
							                <option value="14">2025</option>
							                <option value="15">2026</option>
							                <option value="16">2027</option>
							              </select>
							            </div>
							        </div>
							      </div>
							      <div className="form-group">
							        <div className="col-md-4">
							          <input type="text" className="form-control" name="cvv" id="cvv" placeholder="Security Code"/>
							        </div>
							      </div>
							    </fieldset>
							  </form>
							</div>
					  </div>
			    <Link to="/" className="btn btn-primary pull-right">Complete Signup</Link>
			  </div>
			</div>
	    </div>
    </div>
    );
  }
}

export default BuyItOptions;