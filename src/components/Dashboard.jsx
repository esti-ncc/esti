import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import {confirmation} from '../actions/confirmation';
import Navbar from './Navbar.jsx';
import '../index.css';

class Dashboard extends Component {
  render() {
    return (
    <div className="bg-wallpaper">
    <Navbar />
      	<div className="container dashboard_position">
    		<div className="panel panel-default drop_shadow solid_panel">
    			<div className="text-center"><i className="glyphicon glyphicon-th" aria-hidden="true"></i> Dashboard</div>
      			<div className="panel-body wrapper-xl">
        			<div className="row">
			          <div className="col-md-3">
			            <button className="btn btn-block btn-default text-center dash_icon" onClick={this.props.confirmation}>
			              <i className="fa fa-file-text-o fa-4x" aria-hidden="true"></i>
			              <h5>Create a new estimate</h5>
			            </button>
			          </div>
			          <div className="col-md-3">
			            <label className="btn btn-block btn-default text-center dash_icon btn-file">
			              <i className="fa fa-folder-open fa-4x" aria-hidden="true"></i>
			              <input type="file" className="hidden"/>
			              <h5>Open an existing estimate</h5>
			            </label>
			          </div>
			          <div className="col-md-3">
			            <label className="btn btn-block btn-default text-center dash_icon btn-file">
			              <i className="fa fa-download fa-4x" aria-hidden="true"></i>
			              <input type="file" className="hidden"/>
			              <h5>Import a price list</h5>
			            </label>
			          </div>
			          <div className="col-md-3">
			            <Link to="/price-list" className="btn btn-block btn-default text-center dash_icon">
			              <i className="fa fa-list fa-4x" aria-hidden="true"></i>
			              <h5>View price list(s)</h5>
			            </Link>
			          </div>
        			</div>
      			</div>
    		</div>
  		</div>
    </div>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({confirmation}, dispatch);
}

export default  connect(null, mapDispatchToProps)(Dashboard);