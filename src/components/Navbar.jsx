import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import '../index.css';
import Logo from './../../public/favicon.ico';
import { Link } from 'react-router';
import {confirmation} from '../actions/confirmation';

class Navbar extends Component {
	renderEstimateButton(props) {
		var x = (<Link to="/editor" className="btn btn-info m-r-xs small" title="Continue editing the current estimate">{/*props.titleOfEstimate*/}<i className="fa fa-arrow-right"></i></Link>);
			if (window.location.href.endsWith('editor') || window.location.href.endsWith('new')) {
				x = '';
			}
			return x
	}
  render() {
    return (
    <div>
    	<div className="navbar-fixed-top drop_shadow">
		    <div className="navbar top_nav_2">
		        <img className="logo_img_full" src={Logo}/>
		        <Link to="/" className="m-l-xs btn btn-default m-r-xs" title="Go to the Dashboard"><i className="glyphicon glyphicon-th" aria-hidden="true"></i></Link>
		        <button className="btn btn-default m-r-xs" title="Create a new estimate" onClick={this.props.confirmation}><i className="fa fa-file-text-o" aria-hidden="true"></i></button>
		        <label className="btn btn-default btn-file m-r-xs" title="Open an existing estimate">
		            <i className="fa fa-folder-open" aria-hidden="true"></i>
		            <input className="hidden" type="file"/>
		        </label>
		        {/*<Link disabled className="btn btn-default m-r-xs" title="Edit floor plan"><i className="fa fa-pencil-square-o" aria-hidden="true"></i></Link>*/}
		        <Link to="/settings" className="btn btn-default m-r-xs" title="Settings"><i className="glyphicon glyphicon-cog" aria-hidden="true"></i></Link>
		        <Link className="btn btn-default m-r-xs" title="Print"><i className="glyphicon glyphicon-print" aria-hidden="true"></i></Link>
		        {this.renderEstimateButton(this.props)}
		        <Link className="btn btn-success pull-right download-button m-r-xs" title="Download the current estimate"><i className="glyphicon glyphicon-download-alt" aria-hidden="true"></i></Link>
		    </div>
    	</div>
    </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({confirmation}, dispatch);
}

export default connect(null, mapDispatchToProps)(Navbar);