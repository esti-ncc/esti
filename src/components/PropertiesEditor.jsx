import React, { Component } from 'react';
import '../index.css';
import Navbar from './Navbar.jsx';

class PropertiesEditor extends Component {
  render() {
    return (
    <div>
    	<Navbar />
	    <div className="container m_t_100 col-md-4 col-md-offset-4">
			<div className="panel panel-default drop_shadow solid_panel">
			  <div className="text-center"><i className="fa fa-info-circle" aria-hidden="true"></i> Properties</div>
			  <div className="panel-body wrapper-xl">
			    <form>
				  <div className="form-group">
				    <label className="label">Estimate Name / Price List Name</label>
				    <input type="" className="form-control"/>
				  </div>
				  <div className="form-group">
				    <label className="label">Price List / Field should not appear for price list</label>
				    <select type="" className="form-control" placeholder="Which price list would you like to use?">
				    	<option value="1">San Diego, CA</option>
				    </select>
				  </div>
				  <a href="#" className="btn btn-default width_100">Go back</a>
				  <a href="#" className="btn btn-primary pull-right width_100">Save</a>
				</form>
			  </div>
			</div>
	    </div>
    </div>
    );
  }
}

export default PropertiesEditor;