import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import '../index.css';
import logo from '../images/logo-circle.png';
import {signUpFunction, loginFunction} from '../actions/user_actions';

const tryItButtonInfo = {
	classInfo: 'btn btn-info width_100',
	titleInfo: "Try Estimation Online now for free. Interact in real time. Create an estimate now to see how much time, energy and money this will save you.",
	textInfo: "Try it",
	option: "TRY"
}

const buyItButtonInfo = {
	classInfo: 'btn btn-success m-l-xs width_100',
	titleInfo: "Chose from our competitively priced monthly or yearly payment options",
	textInfo: "Buy it",
	option: "BUY"
}

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isItANewUserBool: false,
			salesOption: null,
			password: "",
			email: "",
			TryOrBuyString: false
		}
	}

	onClickSignUp(e) {
		let user = {email: this.state.email, password: this.state.password};
		e.preventDefault();
		this.props.signUpFunction(user, this.state.salesOption)
	}

	onClickLogin(e) {
		let user = {email: this.state.email, password: this.state.password};
		e.preventDefault();
		this.props.loginFunction(user)
	}

	onClickGoBackToLoginScreen() {
		this.setState({
			isItANewUserBool: false,
			salesOption: null
		})
	}
	renderSalesButtonCreateAccount() {
		return (<button className="btn btn-primary pull-right" onClick={function(e) {this.onClickSignUp(e)}.bind(this)}>Create account</button>)
	}
	renderSignInButton() {
		let results;
		results = this.state.isItANewUserBool ? (this.renderSalesButtonCreateAccount()): (<button className="btn btn-primary width_100 pull-right" onClick={function(e) {this.onClickLogin(e)}.bind(this)}>Sign in</button>);
		return results;
	}

	renderSalesButtons(info) {
		const { classInfo, titleInfo, textInfo, option } = info;
		return <button className={classInfo} title={titleInfo} onClick={function(e) {this.salesOptionFunction(option)}.bind(this)}>{textInfo}</button>
	}

	renderCancelButton() {
		const option1 = (
			<span>
				{this.renderSalesButtons(tryItButtonInfo)}
				{this.renderSalesButtons(buyItButtonInfo)}
			</span>
			)
		const option2 = (
			<button className="btn btn-default width_100" title="Go back to the login screen" onClick={this.onClickGoBackToLoginScreen.bind(this)}>Cancel</button>
			)
		let results;
		results = this.state.isItANewUserBool ? option2: option1;
		return results;
	}

	renderTitleDiv(title) {
		return (<div className="text-center"><i className="fa fa-sign-in" aria-hidden="true"></i>{title}<strong>Estimation Online</strong></div>);
	}

	renderTitleOfLogin() {
		let results;
		results = this.state.isItANewUserBool ? this.renderTitleDiv(" Sign up for ") : this.renderTitleDiv(" Sign in to ");
		return results;
	}

	salesOptionFunction(option) {
		this.setState({salesOption: option, isItANewUserBool: true})
	}

	handleEmailChange(e) {
   this.setState({email: e.target.value});
	}

	handlePasswordChange(e) {
	   this.setState({password: e.target.value});
	}

  render() {
    return (
    <div>
	    <div className="container col-md-4 col-md-offset-4 m_t_100">
			<div className="panel panel-default drop_shadow solid_panel">
			  {this.renderTitleOfLogin()}
			  <div className="panel-body wrapper-xl">
			    <div className="text-center m-b-lg"><img className="logo_on_login_page drop-shadow" src={logo} alt="Estimation Online"/></div>
			    <form>
					  <div className="form-group">
					    <input type="email" className="form-control input-lg bg-grey" placeholder="Email address" value={this.state.email} onChange={this.handleEmailChange.bind(this)} required/>
					  </div>
					  <div className="form-group">
					    <input type="password" className="form-control input-lg bg-grey" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange.bind(this)} required/>
					  </div>
					  {this.renderSignInButton()}
					</form>
					 {this.renderCancelButton()}
			  </div>
			</div>
	    </div>
    </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({signUpFunction, loginFunction}, dispatch);
}

function mapStateToProps(state) {
	return state
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);