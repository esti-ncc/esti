import React, { Component } from 'react';
import { Link } from 'react-router';
import '../index.css';
import Navbar from './Navbar.jsx';

  var lineItem = [{
    sys_id: 123455,
    Item: 'Duct Tape',
    Description: 'strong, cloth-backed, waterproof adhesive tape.',
    UnitType: 'feet',
    UnitPriceCurrency: 'USD',
    UnitPrice: 0.12,
    Quantity: 100,
    Cost: 10
  },{
    sys_id: 123333,
    Item: 'asdfga Tape',
    Description: 'strong, cloth-backed, waterproof adhesive tape.',
    UnitType: 'feet',
    UnitPriceCurrency: 'USD',
    UnitPrice: 0.12,
    Quantity: 100,
    Cost: 10.22
  },{
    sys_id: 127333,
    Item: 'lalala Tape',
    Description: 'strong, cloth-backed, waterproof adhesive tape.',
    UnitType: 'feet',
    UnitPriceCurrency: 'USD',
    UnitPrice: 0.12,
    Quantity: 100,
    Cost: 10.45
  },{
    sys_id: 122333,
    Item: 'lalala sdfgsdfgsdfgsdfg',
    Description: 'strong, cloth-backed, waterproof adhesive tape.',
    UnitType: 'feet',
    UnitPriceCurrency: 'USD',
    UnitPrice: 0.12,
    Quantity: 100,
    Cost: 1234
  }]

class Editor extends Component {
  
  constructor(props) {
    super(props);
    this.state = {Total: 0, 
                  AddItemRow: false,
                  LineItem: lineItem};
  }

  componentWillMount() {
    this.renderTotal(lineItem);
  }

  addItem() {
    var item = {
      sys_id: 188455,
      Item: 'Duct NEW',
      Description: 'strong, cloth-backed, waterproof adhesive tape.',
      UnitType: 'feet',
      UnitPriceCurrency: 'USD',
      UnitPrice: 0.12,
      Quantity: 100,
      Cost: 10
    }
    this.setState({LineItem: this.state.LineItem.concat([item])})
  }

  renderTotal(i) {
    var x = 0;
    i.map(
      function (item) {
        x += item.Cost;
      }
    )
    
    this.setState({Total: x});
  }

  renderSign(amount) {
    let results = amount ? `$${amount}` : '';
    return (<span>{results}</span>)
  }

  renderLineItem(renderSign) {
    if (this.state.LineItem) {
        return this.state.LineItem.map(function(x) {
          return(<tr key={x.sys_id}>
                    <td><input type="checkbox" className="checkbox"/></td>
                    <td>{x.Item}</td>
                    <td>{x.Description}</td>
                    <td>{x.UnitType}</td>
                    <td>{renderSign(x.UnitPrice)}</td>
                    <td>{x.Quantity}</td>
                    <td>{renderSign(x.Cost)}</td>
                  </tr>);
        });
    } else {
      return(<tr/>);
    }
  }

  toggleAddItemRow() {
    this.setState({AddItemRow: !this.state.AddItemRow});
  }

  renderAddItemRow() {
    return(<tr className="bg-ghostwhite">
            <td><button className="btn btn-default" onClick={this.toggleAddItemRow.bind(this)} title="Cancel"><i className="fa fa-times" aria-hidden="true"></i></button></td>
            <td>
              <input className="form-control" list="items" placeholder="Item"/>
              <datalist id="items">
                <option value="Masking Tape"/>
                <option value="Duct Tape"/>
              </datalist>
            </td>
            <td><em className="text-muted small">The description will apear once you have selected an item</em></td>
            <td></td>
            <td></td>
            <td><input className="form-control width_100" type="number" placeholder="Quantity"/></td>
            <td><button className="btn btn-success" onClick={this.addItem.bind(this)}>Add Item</button></td>
          </tr>);
  }
  
  render() {
    return (
      <div>
        <Navbar />
          <div className="panel panel-info b">
            <div className="panel-heading text-center clearfix">
              <Link className="btn btn-default btn-sm heading_btn pull-left" title="Go back" to="/"><i className="fa fa-chevron-left" aria-hidden="true"/></Link>
                <h5 className="heading_title font-bold">Title of Current Estimate
                <Link className="btn btn-danger btn-xs pull-right m-r-sm editor-button" to="/" title="Delete the currently checked item"><span className="hidden-xs padder">Delete</span><i className="fa fa-pencil visible-xs font-20" aria-hidden="true"></i></Link>
                <Link className="btn btn-primary btn-xs pull-right m-r-sm editor-button" to="/" title="Edit the currently checked item in its own window"><span className="hidden-xs padder">Edit</span><i className="fa fa-trash visible-xs font-20" aria-hidden="true"></i></Link>
              </h5>            
            </div>
            <div className="panel-body">
              <table className="table">
                <thead>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <th><span>Item</span></th>
                    <th>Description</th>
                    <th>Unit Type</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                    <th>Cost</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderLineItem(this.renderSign)}
                  {this.state.AddItemRow === true ? this.renderAddItemRow() : <tr/>}
                  <tr>
                    <td></td>
                    <td><strong>Total</strong></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td className="text-success"><strong>{this.renderSign(this.state.Total)}</strong></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="panel-footer clearfix">
              <label className="label text-grey">There are {lineItem.length} line item(s) in this estimate</label>
              <button className="btn btn-default pull-right" onClick={this.toggleAddItemRow.bind(this)} title="Add a new item to this list"href="#"><i className="fa fa-plus" aria-hidden="true"/></button>
            </div>
          </div>
      </div>
    );
  }
}

export default Editor;