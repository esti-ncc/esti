import React, { Component } from 'react';
import { Link } from 'react-router';
import '../index.css';
import Navbar from './Navbar.jsx';

class TryItWelcome extends Component {
  render() {
    return (
    <div>
    	<Navbar />
	    <div className="container col-md-6 col-md-offset-3 m-t-xl">
			<div className="panel panel-default drop_shadow solid_panel">
			  <div className="text-center"><i className="fa fa-globe" aria-hidden="true"></i> Estimation Online</div>
			  <div className="panel-body wrapper-xl">
			    <h1 className="text-primary">Welcome</h1>
			    <hr/>
			    <p>
			    Welcome to Estimation Online. Explore this software for the next thrity days compliments of our team. See how this software can make you more powerful by saving time and increasing productivity. We know that you will love using this tool and look forward to providing you with great service for years to come. 
			    </p>
			    <br/>
			    <em ><strong>- The Estimation Online Team</strong></em>
			    <hr/>
			    <Link to="/" className="btn btn-primary pull-right width_100">Explore</Link>
			  </div>
			</div>
	    </div>
    </div>
    );
  }
}

export default TryItWelcome;