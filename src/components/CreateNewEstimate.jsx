import React, { Component } from 'react';
import {Link} from 'react-router';
import '../index.css';
import Navbar from './Navbar.jsx';

class CreateNewEstimate extends Component {
  render() {
    return (
    <div>
    	<Navbar />
	    <div className="container m_t_100 col-md-4 col-md-offset-4">
			<div className="panel panel-default drop_shadow solid_panel">
			  <div className="text-center"><i className="fa fa-file-text-o" aria-hidden="true"></i> New Estimate</div>
			  <div className="panel-body wrapper-xl">
			    <form>
				  <div className="form-group">
				    <label className="label">Estimate Name</label>
				    <input type="" className="form-control" placeholder="What would you like to call your new estimate?"/>
				  </div>
				  <div className="form-group">
				    <label className="label">Price List</label>
				    <select type="" className="form-control" placeholder="Which price list would you like to use?">
				    	<option value="1">San Diego, CA</option>
				    </select>
				  </div>
				  <Link to="/" className="btn btn-default width_100">Cancel</Link>
				  <Link to="/editor" className="btn btn-primary pull-right width_100">Create</Link>
				</form>
			  </div>
			</div>
	    </div>
    </div>
    );
  }
}

export default CreateNewEstimate;