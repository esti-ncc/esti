import React, { Component } from 'react';
import '../index.css';
import logo from '../images/logo-circle.png';
import rd_logo from '../images/ryandev-logo.png';
import loadSpinner from '../images/spinner.gif';

class LoadingScreen extends Component {
  render() {
    return (
      <div>
        <div id="loadspinner_container" className="container m-t-xl">
          <div className="jumbotron text-center drop_shadow">
            <div><img src={logo} alt="Estimation Online"/></div>
            <div className="wrapper"><span className="">Please wait...</span></div>
            <div><img className="load_spinner" src={loadSpinner} alt="Loading..."/></div>
            <div className="clearfix"></div>
          </div>
        </div>
      </div>
    );
  }
}

export default LoadingScreen;
