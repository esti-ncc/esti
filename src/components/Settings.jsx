import React, { Component } from 'react';
import '../index.css';
import Navbar from './Navbar.jsx';

class Settings extends Component {
  render() {
    return (
    <div>
    	<Navbar />
	    <div className="container col-md-6 col-md-offset-3 m-t-xl">
			<div className="panel panel-default drop_shadow solid_panel">
			  <div className="text-center"><i className="glyphicon glyphicon-cog" aria-hidden="true"></i> Settings</div>
			  <div className="panel-body wrapper-xl">
			    <form>
			    <h5>Application</h5>
			    <div className="row">
					  <div className="form-group col-md-6">
					    <label className="label">Create new line items</label>
					    <div className="radio">
							  <label><input type="radio" name="optradio"/>In the properties panel</label>
							</div>
							<div className="radio">
							  <label><input type="radio" name="optradio"/>In the editor</label>
							</div>
					  </div>
					  <div className="col-md-6">
					    <label className="label">Sign out of Estimation Online</label>
					  	<button className="btn btn-danger btn-block m-t-sm">Sign out</button>
					  </div>
					</div>
				  <hr/>
				  <h5>Account</h5>
				  <div className="col-md-6">
				    <label className="label">Status</label>
				    <h4 className="m-t-sm"><span className="label label-success text-white text-center">Active</span></h4>
				  </div>
				  <div className="form-group col-md-6">
				    <label className="label">Subscription</label>
				    <a href="#" className="btn btn-default btn-block m-t-sm">Manage my subscription</a>
				  </div>
				  <div className="form-group col-md-6">
				    <label className="label">Phone number</label>
				    <input className="form-control" type="tel"/>
				  </div>
				  <div className="form-group col-md-6">
				    <label className="label">Company</label>
				    <input className="form-control" type="text"/>
				  </div>
				  <div className="form-group col-md-6">
				    <label className="label">Email address</label>
				    <input className="form-control" type="text"/>
				  </div>
				  <div className="form-group col-md-6">
				    <label className="label">Password</label>
				    <input className="form-control" type="password"/>
				  </div>
				  <a href="#" className="btn btn-primary pull-right width_100">Save</a>
				</form>
			  </div>
			</div>
	    </div>
    </div>
    );
  }
}

export default Settings;