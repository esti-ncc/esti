import React, { Component } from 'react';
import RyandevLogo from './../images/ryandev-logo.png';
import '../index.css';

class RyandevBrand extends Component {
  render() {
    return (
	    <div>
	    	<div className="ryandev_footer_bar"></div>
    		<div className="pull-right rd_brand">
        	<img className="ryandev_logo" src={RyandevLogo} alt="ryandev" />
    		</div>
	    </div>
    );
  }
}

export default RyandevBrand;