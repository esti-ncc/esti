import React, { Component } from 'react';
import { Link } from 'react-router';
import '../index.css';
import Navbar from './Navbar.jsx';

class PriceListDetail extends Component {
  render() {
    return (
      <div>
          <Navbar />
          <div className="panel panel-default">
            <div className="panel-heading text-center clearfix">
              <Link className="btn btn-default btn-sm heading_btn pull-left" title="Go back" to="/price-list"><i className="fa fa-chevron-left" aria-hidden="true"/></Link>
              <h5 className="heading_title font-bold">San Diego, CA
                <Link className="btn btn-danger btn-xs pull-right m-r-sm editor-button" to="/" title="Delete the currently checked item"><span className="hidden-xs padder">Delete</span><i className="fa fa-pencil visible-xs font-20" aria-hidden="true"></i></Link>
                <Link className="btn btn-primary btn-xs pull-right m-r-sm editor-button" to="/" title="Edit the currently checked item in its own window"><span className="hidden-xs padder">Edit</span><i className="fa fa-trash visible-xs font-20" aria-hidden="true"></i></Link>
              </h5>
            </div>
            <div className="panel-body">
              <table className="table">
                <thead>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <th><span>Item</span></th>
                    <th>Description</th>
                    <th>Unit Type</th>
                    <th>Unit Price</th>
                    <th>Price Region</th>
                    <th>Image</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <td>Duct Tape</td>
                    <td>Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor</td>
                    <td>feet</td>
                    <td>$0.12</td>
                    <td>San Diego, CA</td>
                    <td>img</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="panel-footer clearfix">
              <label className="label text-grey">There are # items in this price list</label>
              <a className="btn btn-default pull-right" title="Add a new item to this list"href="#"><i className="fa fa-plus" aria-hidden="true"/></a>
            </div>
          </div>
      </div>
    );
  }
}

export default PriceListDetail;