import React, { Component } from 'react';
import { Link } from 'react-router';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import {setPriceListSelected, getPriceList} from '../actions/priceList_actions';
import '../index.css';
import Navbar from './Navbar.jsx';
  //
  // var priceList = [{
  //   PriceRegion: 'San Diego, CA',
  //   Description: 'Lorem Ipsum Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Ipsum Dolor',
  //   Source: 'Xactimate',
  //   Year: '2016'
  // },{
  //   PriceRegion: 'Carlsbad, CA',
  //   Description: 'Lorem Ipsum Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Dolor Lorem Ipsum Ipsum Dolor',
  //   Source: 'User Created',
  //   Year: '2017'
  // }]

class PriceList extends Component {
  constructor(props) {
    super(props);
    this.props.getPriceList();
  }
  renderPriceLists() {
    if(this.props.priceList.list) {
      return this.props.priceList.list.map(function(priceList) {
        return (<tr>
          <td><input type="checkbox" className="checkbox"/></td>
          <td><button onClick={function(e) {
            this.props.setPriceListSelected(priceList._id)
          }}></button><i className="fa fa-external-link" aria-hidden="true"></i></td>
          <td>{priceList.region}</td>
          <td>{priceList.description}</td>
          <td>{priceList.source}</td>
          <td>{priceList.date}</td>
        </tr>)
      })
    }
    else {
      return;
    }
  }
  render() {
    return (
      <div>
          <Navbar />
          <div className="panel panel-default">
            <div className="panel-heading text-center clearfix">
              <Link className="btn btn-default btn-sm heading_btn pull-left" title="Go back" to="/"><i className="fa fa-chevron-left" aria-hidden="true"/></Link>
              <h5 className="heading_title font-bold">Price Lists
                <Link className="btn btn-danger btn-xs pull-right m-r-sm editor-button" to="/" title="Delete the currently checked item"><span className="hidden-xs padder">Delete</span><i className="fa fa-pencil visible-xs font-20" aria-hidden="true"></i></Link>
                <Link className="btn btn-primary btn-xs pull-right m-r-sm editor-button" to="/" title="Edit the currently checked item in its own window"><span className="hidden-xs padder">Edit</span><i className="fa fa-trash visible-xs font-20" aria-hidden="true"></i></Link>
              </h5>
            </div>
            <div className="panel-body">
              <table className="table">
                <thead>
                  <tr>
                    <th><input type="checkbox" className="checkbox"/></th>
                    <th></th>
                    <th>Price Region</th>
                    <th>Description</th>
                    <th>Source</th>
                    <th>Year</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderPriceLists()}
                </tbody>
              </table>
            </div>
            <div className="panel-footer clearfix">
              <label className="label text-grey">There are # price lists available for use</label>
              <Link className="btn btn-default pull-right" title="Add a new item to this list" to="#"><i className="fa fa-plus" aria-hidden="true"/></Link>
            </div>
          </div>
      </div>
    );
  }
}


function mapStateToProps({priceList}) {
  return {
    priceList
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({setPriceListSelected, getPriceList}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(PriceList);
