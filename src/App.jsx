import React, { Component } from 'react';
import {connect} from 'react-redux';
import {fetchUser} from './actions/user_actions';
import Navbar from './components/Navbar.jsx';
import RyandevBrand from './components/RyandevBrand.jsx';
import BLANK from './BLANK.jsx';

class App extends Component {
  componentDidMount() {
    if (localStorage.getItem("token") && !this.props.authenticated) {
      this.props.fetchUser();
    }
  }
	renderNavbar() {
		let results;
		results = this.props.authenticated ? (<Navbar />) : <BLANK />;
		return results
	}
  render() {
    return (
      <div>
        {this.renderNavbar()}
        {this.props.children}
        <RyandevBrand />
      </div>
    );
  }
}

function mapStateToProps({auth}) {
	return {authenticated: auth.authenticated}
}
export default connect(mapStateToProps, {fetchUser})(App);
