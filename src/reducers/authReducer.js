import {
	AUTH_USER, UNAUTH_USER, AUTH_USER_ERROR, FETCH_USER
		} from "../actions/types";

export default function(state = {}, action) {
	switch(action.type) {
		case AUTH_USER:
			return {
				...state,
				error: null,
				authenticated: true,
				user: {}
			};
		case UNAUTH_USER:
			return {
			...state,
			error: null,
			authenticated: false,
			user: {}
		};
		case AUTH_USER_ERROR:
			return {
			...state,
			error: true,
			authenticated: false,
			user: {}
		};
		case FETCH_USER:
			return {
			...state,
			error: false,
			authenticated: true,
			user: action.payload
		};
	}
	return state;
}
