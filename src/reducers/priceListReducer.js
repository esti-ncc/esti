import {
	FETCH_DEFAULT_PRICELIST_ERROR, FETCH_DEFAULT_PRICELIST, SET_PRICELIST_SELECTED
		} from "../actions/types";

export default function(state = {}, action) {
	switch(action.type) {
		case FETCH_DEFAULT_PRICELIST_ERROR:
			return {
				...state,
				error: true
			};
		case FETCH_DEFAULT_PRICELIST:
			return {
			error: null,
			selected: null,
			list: action.payload
		};
		case SET_PRICELIST_SELECTED:
			return {
			...state,
			selected: action.payload
		};
	}
	return state;
}
