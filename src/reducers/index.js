import { combineReducers } from "redux";
import { reducer } from "redux-form";
import authReducer from "./authReducer";
import priceListReducer from "./priceListReducer";

const rootReducer = combineReducers({
	auth: authReducer,
	priceList: priceListReducer
})
export default rootReducer;
