import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import reduxThunk from "redux-thunk";
import {Provider} from "react-redux";
import {createStore, applyMiddleware } from 'redux';
import App from './App.jsx';
import './index.css';
import Dashboard from './components/Dashboard.jsx';
import LoadingScreen from './components/LoadingScreen.jsx';
import PriceList from './components/PriceList.jsx';
import PriceListDetail from './components/PriceListDetail.jsx';
import CreateNewEstimate from './components/CreateNewEstimate.jsx';
import Editor from './components/Editor.jsx';
import PropertiesEditor from './components/PropertiesEditor.jsx';
import Login from './components/Login.jsx';
import Settings from './components/Settings.jsx';
import TryItWelcome from './components/TryItWelcome.jsx';
import ButItOptions from './components/BuyItOptions.jsx';
import reducers from "./reducers"

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const app = document.getElementById('root');

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <Router history={browserHistory}>
    	<Route path="/" component={App}>
        <IndexRoute component={Dashboard} />
      	<Route path="/loading" component={LoadingScreen} />
      	<Route path="/price-list" component={PriceList} />
      	<Route path="/price-list-detail" component={PriceListDetail} />
        <Route path="/new" component={CreateNewEstimate} />
        <Route path="/editor" component={Editor} />
        <Route path="/props" component={PropertiesEditor} />
        <Route path="/login" component={Login} />
        <Route path="/settings" component={Settings} />
        <Route path="/tryitwelcome" component={TryItWelcome} />
        <Route path="/buyitoptions" component={ButItOptions} />
      </Route>
    </Router>
  </Provider>,
app);
