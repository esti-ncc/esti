const newFunction = require("./new");
const deleteFunction = require("./delete");
const editFunction = require("./edit");

const itemListFNs = {
  new: newFunction,
  edit: editFunction,
  del: deleteFunction,
}
module.exports = itemListFNs
