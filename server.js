const express = require('express');
const http = require('http');
const bodyParser = require("body-parser");
const morgan = require('morgan');
const app = express();
const UserRouter = require('./server/routes/router_user');
const EstimateRouter = require('./server/routes/router_estimate');
const VendorRouter = require('./server/routes/router_vendor');
const PriceListRouter = require('./server/routes/router_priceList');
const PriceRouter = require('./server/routes/router_price');
const db = require('./server/database');
const cors = require('cors');
app.use(morgan('combined'));
app.use(cors()); // Allows CORS(Cross Origin GET Request)
app.use(bodyParser.json({type: '*/*'}));
app.use(express.static('public'))
UserRouter(app);
EstimateRouter(app);
VendorRouter(app);
PriceRouter(app);
PriceListRouter(app);





const port = process.env.PORT || 3000;
// const server = http.createServer(app);
app.listen(port, function() {
  console.log('Server Listening on localhost:' + port);
});

module.exports = app;
